import seaborn as sn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from matplotlib.gridspec import GridSpec
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import pairwise_distances_argmin_min
from medangel.models.model_builder import kmeans_clustering
from medangel.features.feature_creation import extract_minimum_features

def create_heatmap(extracted_features):

    scaler = StandardScaler()
    scaled_features = scaler.fit_transform(extracted_features)
    scaled_df = pd.DataFrame(scaled_features, columns = extracted_features.columns)
    covMatrix = pd.DataFrame.cov(scaled_df)
    sn.heatmap(covMatrix,annot=True)


def create_multiple_figure(df_relevant):
    extracted_features = extract_minimum_features(df_relevant)
    kmeans, scaled_features = kmeans_clustering(extracted_features)
    closest, _ = pairwise_distances_argmin_min(kmeans.cluster_centers_, scaled_features)
    plot_ids = extracted_features.iloc[closest].index
    clusters = kmeans.predict(scaled_features)
    plot1_data = df_relevant[df_relevant["doy_id"] == plot_ids[0]]
    plot2_data = df_relevant[df_relevant["doy_id"] == plot_ids[1]]
    plot3_data = df_relevant[df_relevant["doy_id"] == plot_ids[2]]
    fig = plt.figure(figsize=(8, 6))
    gs = GridSpec(3, 2)
    scaled_df = pd.DataFrame(scaled_features, columns = extracted_features.columns)
    ax0 = plt.subplot(gs[0:3,0])
    ax0.scatter(x=extracted_features.temperature__minimum, y=extracted_features.temperature__standard_deviation, c = clusters, cmap = cm.Set1)
    plt.xlabel("temperature__minimum")
    plt.ylabel("temperature__standard_deviation")
    ax0.set_title("Plot Showing Clusters")

    ax1 = plt.subplot(gs[0,1])
    ax1.plot(plot1_data.datetime, plot1_data.temperature, "red")
    ax1.set_title(str(plot_ids[0]) + " Example of data from cluster 0")
    plt.xlabel("Time")
    plt.ylabel("Temperature (C)")
    plt.setp(ax1.get_xticklabels(), rotation=30, horizontalalignment='right')
    ax2 = plt.subplot(gs[1,1])
    ax2.plot(plot2_data.datetime, plot2_data.temperature, "orange")
    ax2.set_title(str(plot_ids[1]) + " Example of data from cluster 1")
    plt.xlabel("Time")
    plt.ylabel("Temperature (C)")
    plt.setp(ax2.get_xticklabels(), rotation=30, horizontalalignment='right')
    ax3 = plt.subplot(gs[2,1])
    ax3.plot(plot3_data.datetime, plot3_data.temperature,"grey")
    ax3.set_title(str(plot_ids[2]) + " Example of data from cluster 2")
    plt.xlabel("Time")
    plt.ylabel("Temperature (C)")
    plt.setp(ax3.get_xticklabels(), rotation=30, horizontalalignment='right')
    
    plt.tight_layout()
    plt.savefig('cluster_example.pdf', bbox_inches='tight', dpi = 300)
    plt.show()