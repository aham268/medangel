import pandas as pd
import tsfresh
from medangel import raw_data_path
from pathlib import Path
from medangel.features import feature_creation as fc

def load_in_data():

    file_path = Path(raw_data_path) / "CSV Fridges data.csv"
    df = pd.read_csv(file_path)



    return df
    
    
def clean_data(df):
    # This function should be used after loading in the data using "load_in_data". It cleans the data that comes out of the sensor 
    # and converts it to an editable version.

        
    df["device_id"] = df["device_id"].astype(int)
    df["temperature"] = df["temperature"].apply(lambda x: x.replace(",","."))
    df["temperature"] = df["temperature"].astype('float64') 
    df["datetime"] = pd.to_datetime(df["datetime"], format = "%d.%m.%Y %H:%M:%S")
    df["datetime"] = pd.to_datetime(df["datetime"])

    periods = df["datetime"].apply(lambda x: pd.Period(x,freq = "D").dayofyear)
    year = df["datetime"].apply(lambda x: pd.Period(x,freq = "D").year)

    df["doy"] = periods
    df["year"] = year

    doy_id = df["device_id"].astype("str") + ":" + df["year"].astype("str") + ":" + df["doy"].astype("str")

    df["doy_id"] = doy_id
    return df


def select_relevant_data(df):
    # This function should be used on the output of the "clean_data" function. It selects the most important columns and outputs 
    # them into an new dataset

    dict_temp = {'doy_id':df["doy_id"], "temperature":df["temperature"], "datetime": df["datetime"], "device_id": df["device_id"]}
    df_relevant = pd.DataFrame(dict_temp)
    count_table = df_relevant.groupby("doy_id")["doy_id"].count()
    drop_ids = count_table[count_table > 486].index
    df_relevant = df_relevant[~df_relevant["doy_id"].isin(drop_ids)]
    return df_relevant

def load_relevant_data():

    #this loads in the original dataset after it has been cleaned


    df = load_in_data()
    df = clean_data(df)
    df_relevant = select_relevant_data(df)
    return df_relevant


def load_device_ids():

    # This function loads the useable data sets and exclude the devices with too many gaps in them

    devices = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\device_ids.h5',key='id')
    devices = devices.drop(devices[(devices == 49103) | (devices == 47683)].index)
    return devices

def load_feature_data():

    # the feature data set is split apart to ensure thaat it is easier to access separate devices easier. This function helps load in the data from all the useable devices
    features = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_features.h5',key='f')
    features = features.drop([49103,47683],level = 0)
    y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_59497_20.h5', key='y')
    y_shifted = y_shifted.dropna()
    devices = load_device_ids()
    for device in devices:
        if device == 59497:
            
            continue
        
        temp_y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '_20.h5', key='y')
        temp_y_shifted = temp_y_shifted.dropna()
        y_shifted = y_shifted.append(temp_y_shifted)
    y = y_shifted

    return features, y

def is_gapless(df: pd.DataFrame):
    ## This function takes in a pandas dataframe that should have a "datetime" column and checks if it has any gaps larger than 5 minutes within the data.
    
    # Ensures "datetime" column is in datetime format
    df["datetime"] = pd.to_datetime(df["datetime"])

    # sort the values in order to use diff function
    df = df.sort_values(by=["datetime"])

    # reset the indices to clean up the data
    df = df.reset_index(drop=True)

    # calculate the gap between 2 readings using diff function
    df["gap"] = df["datetime"].diff().fillna(pd.Timedelta(0))

    # this is a binary array checking if a gap is too large
    binary = df.gap >= pd.Timedelta(10, unit = 'min')

    if sum(binary) > 0:
        return False
    else:
        return True
    
def device_cleanup(df: pd.DataFrame):
    # Takes a raw dataset from the new devices provided by Amin and cleans them
    df["datetime"] = pd.to_datetime(df["datetime"])
    df = df.sort_values(by=["datetime"])
    df = df.reset_index(drop=True)
    return df
