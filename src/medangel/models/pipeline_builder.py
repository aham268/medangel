import pandas as pd
import numpy as np
import tsfresh
from sklearn.base import TransformerMixin


class CollinearityTransformer(TransformerMixin):
    
    def transform(self,X,y=None):
        if not isinstance(X, pd.DataFrame):
            X = pd.DataFrame(X)
        
        corr_matrix = X.corr().abs()
        upper_tri = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(np.bool))
        to_drop = [column for column in upper_tri.columns if any(upper_tri[column] > 0.95)]
        return X.drop(to_drop, axis=1)


if __name__ == "__main__":
    df_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_52040_20.h5'
    df = pd.read_hdf(df_directory,key='df')
    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()
    features = tsfresh.feature_extraction.extract_features(df, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value = "temperature")
    transformer = CollinearityTransformer()
    features_transformed = transformer.transform(features)
    print(features_transformed.head(10))
