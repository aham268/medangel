import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler

def kmeans_clustering(extracted_features):

    scaler = StandardScaler()
    scaled_features = scaler.fit_transform(extracted_features)

    kmeans = KMeans(init = "random",
                n_clusters = 3,
                n_init = 10,
                random_state = 42
                )

    kmeans.fit(scaled_features)
    
    return kmeans, scaled_features


