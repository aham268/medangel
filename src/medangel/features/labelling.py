import pandas as pd

def rolling_labels(rolled_id, df_rolled):
    temp = df_rolled[df_rolled["id"] == rolled_id]
    if temp["temperature"].min() <= 0:
        return "freezing"
    elif temp["temperature"].max() <= 2:
        return "cold"
    elif temp["temperature"].max() >= 25:
        return "hot"
    elif temp["temperature"].min() >= 8:
        return "warm"
    else:
        return "ideal"


def temperature_labels(df):

    df.loc[df['temperature']<=0,'label'] = 'Freezing'
    df.loc[df['temperature'].between(0,2),'label'] = 'Cold'
    df.loc[df['temperature'].between(2,8),'label'] = 'Ideal'
    df.loc[df['temperature'].between(8,25),'label'] = 'Warm'
    df.loc[25 < df['temperature'],'label'] = 'Hot'
    return df

def doy_labels(doy,device_id,df):

    temp = df[df["device_id"] == device_id]
    temp2 = temp[temp["doy"] == doy]
    temp2.head()
    print(temp2["temperature"].min())
    if temp2["temperature"].min() <= 0:
        return "code1"
    elif len(temp2[temp2["temperature"] <= 2].index) > 10:
        return "code2"
    elif temp2["temperature"].max() >= 25:
        return "code4"
    elif len(temp2[temp2["temperature"] >= 8].index) > 10:
        return "code3"
    else:
        return "ideal"        


def rolling_labels_speed(rolled_id, y, df_rolled):
    y['label'] = 'ideal'
    y.loc[(df_rolled[df_rolled["id"] == rolled_id].temperature.min()<=0),'label'] = 'freezing'
    return y['label']