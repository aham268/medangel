import pandas as pd
import tsfresh
from tsfresh.utilities.dataframe_functions import roll_time_series
from medangel.data import preprocessing

def extract_minimum_features(df_relevant):
    settings =  tsfresh.feature_extraction.MinimalFCParameters()
    extracted_features = tsfresh.feature_extraction.extract_features(df_relevant, 
                                              column_id = "doy_id",
                                              column_sort = "datetime",
                                              default_fc_parameters=settings,
                                              column_value = "temperature")

    extracted_features = extracted_features.drop(columns = "temperature__length")
    assert(len(extracted_features == 1104))
    return extracted_features

def extract_comprehensive_features(df_relevant):    
    # Needs to be corrected to remove days with duplicate readings -- DONE THROUGH PREPROCESSING.PY
    
    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()
    comprehensive_features = tsfresh.feature_extraction.extract_features(df_relevant, 
                                                column_id = "doy_id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value = "temperature")

    
    

    nan_columns = comprehensive_features.columns[comprehensive_features.isna().any()].tolist()
    comprehensive_features = comprehensive_features.drop(columns=nan_columns)

    return comprehensive_features


def feature_creation(df: pd.DataFrame, col_id = "id", col_sort ="order", col_value="temperature") -> pd.DataFrame:

    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()
    features = tsfresh.feature_extraction.extract_features(df,
                                                column_id = col_id,
                                                column_sort = col_sort,
                                                default_fc_parameters=settings,
                                                column_value = col_value,
                                                n_jobs=4)

    return features

def rolled_feature_creation(df: pd.DataFrame, col_id="device_id",interval = 99)-> pd.DataFrame:
    df = df.sort_values("datetime")
    df["order"] = range(df.shape[0])
    df_rolled = roll_time_series(df,
                                column_sort="order", 
                                column_id="device_id",
                                max_timeshift=interval,
                                min_timeshift=interval)
    features = feature_creation(df_rolled)

    return features

def label_creation(df:pd.DataFrame, col_id="device_id",interval = 9) -> pd.DataFrame:

    #This function takes in a dataframe that must have 3 columns: "datetime", "device_id" and "temperature"
    #The function then splits the dataframe into 10 reading chunks (or any size chunk when changing interval parameter)
    #It then labels each chunk by:- "warm": if there are 10 readings in the chunk that are greater than 8 degrees
    #-"hot": if there is a single reading above 25 degrees in the chunk
    #-"cold":if there are 10 readings in the chunk that are less than 2 degrees
    #-"freezing": if there is a single reading below 0 degrees in the chunk
    #-"ideal": does not breach any of the thresholds above

    # cleanup the data set
    df = preprocessing.device_cleanup(df)

    # sort by datetime to order the values
    df = df.sort_values("datetime")
    df["order"] = range(df.shape[0])

    # roll the time series into chunks
    df_rolled = roll_time_series(df,column_sort="order", column_id=col_id,max_timeshift=interval,min_timeshift=interval)

    # this method labels the data using a binary method
    rolled_ids = df_rolled["id"].value_counts().index
    y = pd.DataFrame(rolled_ids, columns = ["roll_id"])
    freezing_ids = df_rolled.groupby(['id']).min().temperature <= 0

    cold_indeces = df_rolled["temperature"].between(0,2)
    cold_df = df_rolled[cold_indeces].groupby(['id']).count() >= 10
    cold_ids = cold_df[cold_df["temperature"] == True].index
    warm_indeces = df_rolled["temperature"].between(8,25)
    warm_df = df_rolled[warm_indeces].groupby(['id']).count() >= 10
    warm_ids = warm_df[warm_df['temperature'] == True].index
    hot_ids = df_rolled.groupby(['id']).max().temperature >= 25
    
    # this tells us what was the last reading in the chunk and we then shift the data the size of the chunk backwards in order to align with the feature interval 
    # which gives a label that describes the next interval
    y['seq_id'] = y['roll_id'].astype(str).str.split(r'\D',expand = True)[3].astype('int')
    y = y.sort_values("seq_id",ascending = True)
    y = y.set_index('roll_id')
    y["label"] = "ideal"
    y.loc[warm_ids,"label"] = "warm"
    y.loc[cold_ids,"label"] = "cold"
    y.loc[hot_ids,"label"] = "hot"
    y.loc[freezing_ids,"label"] = "freezing"

    y["label_shifted"] = y["label"].shift(-(interval+1))
    y["seq_shifted"] = y["seq_id"].shift(-(interval+1))

    return y

def extract(temp, permanent):
    temp = preprocessing.device_cleanup(temp)
    features = rolled_feature_creation(temp)
    permanent = permanent.append(temp)
    return permanent