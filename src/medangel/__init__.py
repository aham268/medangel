import sys

if sys.version_info[:2] >= (3, 8):
    # TODO: Import directly (no need for conditional) when `python_requires = >= 3.8`
    from importlib.metadata import PackageNotFoundError, version  # pragma: no cover
else:
    from importlib_metadata import PackageNotFoundError, version  # pragma: no cover

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = "Medangel"
    __version__ = version(dist_name)
except PackageNotFoundError:  # pragma: no cover
    __version__ = "unknown"
finally:
    del version, PackageNotFoundError


import joblib
import os
import pkg_resources

__all__ = ['data', 'models', 'features', 'visualization']

try:
    __version__ = pkg_resources.get_distribution(__name__).version
except:
    __version__ = 'unknown'

module_path = os.path.dirname(os.path.abspath(__file__))
src_path = os.path.normpath(os.path.join(module_path, '..'))
project_path = os.path.normpath(os.path.join(src_path, '..'))
models_path = os.path.join(project_path, 'models')
reports_path = os.path.join(project_path, 'reports')
figures_path = os.path.join(reports_path, 'figures')
data_path = os.path.join(project_path, 'data')
raw_data_path = os.path.join(data_path, 'raw')
new_data_path = os.path.join(raw_data_path,'new_data')
preprocessed_path = os.path.join(data_path, 'preprocessed')