import pandas as pd
from medangel.data import preprocessing

df = preprocessing.load_relevant_data()
devices = df["device_id"].value_counts()
devices.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\device_ids.h5',key='id')