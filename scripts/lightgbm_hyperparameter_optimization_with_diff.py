import pandas as pd
import numpy as np

from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation

from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import RandomizedSearchCV

from imblearn.pipeline import Pipeline
from imblearn.under_sampling import RandomUnderSampler

from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.transformers import FeatureSelector

from lightgbm import LGBMClassifier

import re

import time

if __name__ == '__main__':

    current_time = time.time()
    devices = preprocessing.load_device_ids()
    features,y = preprocessing.load_feature_data()
    features = features.drop([49103,47683],level = 0)
    y = y.loc[features.index]
    features_diff = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\features_with_diff.h5',key='df')
    features_diff = features_diff.dropna(axis=1)
    features_diff = features_diff.loc[features.index]
    features = pd.concat([features, features_diff], axis=1, join="inner")

    
    num_leaves = [10,20,40,60,80,100]
    max_depth = [6,7,8,9,10]
    boosting_type = ['gbdt','dart','goss']
    n_estimators = [100,250,500,1000,1500]
    fdr_level = [0.01,0.02,0.025,0.03,0.04]


    lgbm_clf = Pipeline([
        ('sampler',RandomUnderSampler(random_state=42)),
        ('augmenter',FeatureSelector()),
        ('classifier',LGBMClassifier(random_state=42,n_jobs=-1,device_type = 'gpu',max_bin = 64)),
    ])


    grid_param = {'augmenter__fdr_level':fdr_level,
                'classifier__num_leaves':num_leaves,
                'classifier__max_depth':max_depth,
                'classifier__boosting_type':boosting_type,
                'classifier__n_estimators':n_estimators}

    rsCV = RandomizedSearchCV(estimator=lgbm_clf,
                            param_distributions = grid_param,
                            n_iter=50,
                            cv=5,
                            random_state=42,
                            n_jobs = 3)
                            
    x_array = np.array(features.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))
    rsCV.fit(x_array,y["label_shifted"])

    print(rsCV.best_params_)
    print(time.time() - current_time)
