def sort(arr) -> list:
    if len(arr) <= 1:
        return arr
    
    left =[]
    right=[]
    equal=[]
    pivot = arr[-1]

    for num in arr:
        if num < pivot:
            left.append(num)
        elif num == pivot:
            equal.append(num)
        else:
            right.append(num)
    
    return sort(left) + equal + sort(right)

import random

if __name__ == '__main__':

    arr = [23,12,6,112,13,25,36,77,523,1231,23,413,53]
    sorted_arr = sort(arr)
    print(sorted_arr)


    randomlist = []
    for i in range(100):
        n = random.randint(1,30)
        randomlist.append(n)
    print(randomlist)
    sorted_random_list = sort(randomlist)
    print(sorted_random_list)