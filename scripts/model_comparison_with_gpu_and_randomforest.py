import pandas as pd
import numpy as np

from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation

from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import LeaveOneGroupOut
from sklearn.metrics import matthews_corrcoef

from imblearn.pipeline import Pipeline
from imblearn.under_sampling import RandomUnderSampler

from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.transformers import FeatureSelector

from lightgbm import LGBMClassifier

import re

import time

from tqdm import tqdm

if __name__ == '__main__':
    # initializing arrays that will be used to build final dataframe
    scores = []
    times = []
    title = []
    test_group = []
    fit_times = []
    pred_times = []

    features,y = preprocessing.load_feature_data()

    classifier__num_leaves = 80
    classifier__n_estimators = 250
    classifier__max_depth = 7
    classifier__boosting_type = 'goss'
    augmenter__fdr_level = 0.03


    lgbm_gpu = Pipeline([
        ('sampler',RandomUnderSampler(random_state=42)),
        ('augmenter',FeatureSelector(fdr_level=augmenter__fdr_level,n_jobs=5)),
        ('classifier',LGBMClassifier(random_state=42,n_jobs=5,device_type = 'gpu',max_bin = 64,num_leaves=classifier__num_leaves,
        n_estimators=classifier__n_estimators,max_depth=classifier__max_depth,boosting_type=classifier__boosting_type)),
    ])

    lgbm = Pipeline([
        ('sampler',RandomUnderSampler(random_state=42)),
        ('augmenter',FeatureSelector(fdr_level=augmenter__fdr_level,n_jobs=5)),
        ('classifier',LGBMClassifier(random_state=42,n_jobs=5,max_bin = 64,num_leaves=classifier__num_leaves,
        n_estimators=classifier__n_estimators,max_depth=classifier__max_depth,boosting_type=classifier__boosting_type)),
    ])

    rf= Pipeline([
                    ('sampler',RandomUnderSampler(random_state=42)),
                    ('augmenter',FeatureSelector(fdr_level=augmenter__fdr_level,n_jobs=5)),
                    ('classifier',RandomForestClassifier(random_state=42, n_jobs=2)),
                    ])

    
    y_temp = y.loc[y.index.isin(features.index)]
    features = features.loc[y_temp.index]
    x_array = np.array(features.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))
    y_array = np.array(y_temp["label_shifted"])

    groups = np.array([value[0] for value in features.index])

    logo = LeaveOneGroupOut()

    for i in tqdm(range(25), desc="first loop"):
        for train_index,test_index in logo.split(features,y_temp,groups):
            start = time.time()

            x_train,x_test = x_array[train_index],x_array[test_index]
            y_train,y_test = y_array[train_index],y_array[test_index]

            test_device = list(set(groups[test_index]))

            lgbm_gpu.fit(x_train,y_train)
            fit_times.append(time.time() - start)

            start = time.time()

            pred = lgbm_gpu.predict(x_test)
            pred_times.append(time.time() - start)

            scores.append(matthews_corrcoef(y_test,pred))

            title.append("gpu")
            test_group.append(test_device[0])


            lgbm.fit(x_train,y_train)
            fit_times.append(time.time() - start)

            start = time.time()

            pred = lgbm.predict(x_test)
            pred_times.append(time.time() - start)

            scores.append(matthews_corrcoef(y_test,pred))

            title.append("no_gpu")
            test_group.append(test_device[0])

            rf.fit(x_train,y_train)
            fit_times.append(time.time() - start)

            start = time.time()

            pred = rf.predict(x_test)
            pred_times.append(time.time() - start)

            scores.append(matthews_corrcoef(y_test,pred))

            title.append("random_forest")
            test_group.append(test_device[0])

    
    df_dict = {'title':title,
            'test_device':test_group,
            'scores':scores,
            'fit_times':fit_times,
            'pred_times':pred_times}
    df_scores = pd.DataFrame(df_dict)
    print(df_scores.head(10))
    df_scores.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\prediction_interval_comparison_data.h5',key="df")