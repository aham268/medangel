import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestClassifier
from imblearn.pipeline import Pipeline
from tsfresh.transformers import FeatureSelector
from sklearn.model_selection import RepeatedKFold
from sklearn.metrics import matthews_corrcoef
from lightgbm import LGBMClassifier
import time
import re



if __name__ == '__main__':

    
    # Loading data and setting up pipelines
    df = preprocessing.load_relevant_data()
    features_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_features.h5'
    features = pd.read_hdf(features_directory, key='f')
    rf_clf = Pipeline([
                    ('sampler',RandomUnderSampler(random_state=42)),
                    ('augmenter',FeatureSelector()),
                    ('classifier',RandomForestClassifier(random_state=42, n_jobs=2)),
                    ])

    rkf = RepeatedKFold(n_splits=10, n_repeats=5, random_state=42)

    print(features.shape)
    features.head(10)
    lgbm_clf = Pipeline([
                        ('sampler',RandomUnderSampler(random_state=42)),
                        ('augmenter',FeatureSelector()),
                        ('classifier',LGBMClassifier(random_state=42, n_jobs=-1,device_type = 'gpu',max_bin = 64)),
                        ])
                        
    # Preparing Loop to go through each device as well as each timeseries. 
    # Should create this as a function rather than a loop 
    devices = df["device_id"].value_counts()
    time_series = [80,60,40,20]
    
    # initializing arrays that will be used to build final dataframe
    scores = []
    times = []
    title = []
    method = []

    # Loop throught each timestamp
    for time_stamp in time_series:
        # Naming conventions for timestamps is different than original 5 hour interval naming convention. 
        # This is an issue which should be solved but this if statement deals with it
        if time_stamp is not 100:
            y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_59497_' + str(time_stamp) + '.h5', key='y')
        else:
            y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_59497.h5', key='y')
        
    # Loop through each device in the data in order to add it to the y matrix
        for device in devices.index:

            print("Working on device: " + str(device))
            if device == 59497:
                print("passing 59497")
                continue
            
            # Solving time stamp naming convention issue
            if time_stamp is not 100:
                temp_y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '_' + str(time_stamp) + '.h5', key='y')
            else:
                temp_y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '.h5', key='y')
            
            temp_y_shifted = temp_y_shifted.dropna()
            y_shifted = y_shifted.append(temp_y_shifted)
        
        
        y_shifted=y_shifted.loc[features.index]
        
        # Changing y matrix to numpy type to help with better runtime and solve any issues with Pandas compatability
        y = y_shifted["label_shifted"].to_numpy()

        # Looping through each combination of train and test indeces
        for train_index, test_index in rkf.split(features):

            # Recording start time of the Random Forest pipeline
            start = time.time()

            # Splitting data into train and test
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = y[train_index], y[test_index]

            # Fitting the RF pipeline
            rf_clf.fit(X_train, y_train)

            # Predicting using test data
            pred = rf_clf.predict(X_test)

            # Recording end of this whole process
            end = time.time()

            # appending the score of the prediction and time.
            scores.append(matthews_corrcoef(y_test,pred))
            times.append(end-start)
            title.append(str(time_stamp*3) +'_minute_prediction_horizon')
            method.append("random_forest")

            # Recording start time of the lightgbm pipeline
            start = time.time()

            # This code solves compatability issues LGBM has with pd dataframes
            X_train = np.array(X_train.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))
            X_test = np.array(X_test.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))

            # Fitting LGBM pipeline
            lgbm_clf.fit(X_train,y_train)

            # Predicitng using lgbm pipeline
            pred_lgbm = lgbm_clf.predict(X_test)

            # Recording endtime and adding it to matrix
            end = time.time()
            scores.append(matthews_corrcoef(y_test,pred_lgbm))
            times.append(end-start)
            title.append(str(time_stamp*3) +'_minute_prediction_horizon')
            method.append("light_gbm")
        


    # Building matrix of all data collected on the pipelines and saving it.
    df_dict = {
        "title":title,
        "score":scores,
        "time":times,
        "method":method
        }
    violin_data = pd.DataFrame(df_dict)
    violin_data.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\violin_data.h5',key="df")
            