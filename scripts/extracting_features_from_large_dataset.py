import pandas as pd
from medangel.data import preprocessing as pp
from medangel.models import model_builder
from medangel.features import feature_creation as fc
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series
import glob
import os
from pathlib import Path
from tqdm import tqdm
from medangel import new_data_path
from medangel import preprocessed_path

if __name__ == '__main__':
    ### This script helps load in all the new data and find the datasets without any large gaps then uses those to extract new features

    DIR = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\raw\new data'
    # Here I am setting up the tracker dataset which helps track which devices have a maximum gap of 10 mins
    names = [Path(x).stem for x in glob.glob(DIR + r'\*.csv')]
    files = glob.glob(DIR + r'\*.csv')
    tracker_dict = {"device":names,"file_name":files}
    tracker = pd.DataFrame(tracker_dict)
    tracker["gapless"] = True

    # This for loop goes through each file, opens it and tests if there are gaps.
    for i in tqdm(range(len(files))):
        df = pd.read_csv(files[i])
        tracker.loc[i,"gapless"] = pp.is_gapless(df)

    print("Total number of gapless datasets = {}".format(tracker.gapless.sum()))

    gapless_files = tracker[tracker["gapless"] == True].file_name

    features = pd.DataFrame()
    for file in tqdm(gapless_files):
        temp = pd.read_csv(file)
        temp["device_id"] = int(tracker.iloc[tracker[tracker.file_name == file].index[0]]["device"])
        features = fc.extract(temp,features)

    features_path = os.path.join(preprocessed_path, 'new_data_features.h5')
    features.to_hdf(features_path,key='df')
    features = ''

    y = pd.DataFrame()
    for file in tqdm(gapless_files):
        temp = pd.read_csv(file)
        temp["device_id"] = int(tracker.iloc[tracker[tracker.file_name == file].index[0]]["device"])
        y_temp = fc.label_creation(temp,interval=19)
        y = y.append(y_temp)

    y_path = os.path.join(preprocessed_path, 'new_data_labels.h5')
    y.to_hdf(y_path,key='df')
    y = ''