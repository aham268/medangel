import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series
import tsfresh

settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()


if __name__ == '__main__':
    df = preprocessing.load_relevant_data()

    devices = df["device_id"].value_counts()

    df_rolled = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_59497.h5', key='df')
    y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_59497.h5', key='y')
    features = tsfresh.feature_extraction.extract_features(df_rolled, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value = "temperature")
    y_shifted = y_shifted.dropna()
    features = features.loc[y_shifted.index]

    for device in devices:
        temp_df_rolled = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_' + str(device) + '.h5', key='df')
        temp_y_shifted = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '.h5', key='y')
        temp_features = tsfresh.feature_extraction.extract_features(temp_df_rolled, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value = "temperature")
        temp_y_shifted = temp_y_shifted.dropna()
        temp_features = features.loc[y_shifted.index]