import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series

if __name__ == '__main__':

    #Loading in and Cleaning data
    df = preprocessing.load_relevant_data()

    #Preparing Loop to go through each device
    devices = df["device_id"].value_counts()
    time_series = [19]
    for time in time_series:
    #Loop through each device in the data
        for device in devices.index:

            #Intialize save directories for rolled dataframe and labels.
            df_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_' + str(device) + '_' + str(time+1) + '.h5'
            y_5hr_dir = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '.h5'
            y_var_dir = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '_' + str(time+1) + '.h5'

            print("Working on device: " + str(device))
            
            #temperory df of only values corresponding to the correct device
            df_temp = df[df["device_id"] == device]

            #Sorting values based on datetime, this is to add an "Order" column because tsfresh seems to benefit from this rather than just having a sorted column
            df_temp = df_temp.sort_values("datetime")
            df_temp["order"] = range(df_temp.shape[0])
            print("Rolling the data:")

            #Roll the time series using tsfresh functionality. I use a max_timeshift and min_timeshift of 99 because this makes the timeseries roll only 100 values (index begins at 0)
            df_rolled = roll_time_series(
                                        df_temp,
                                        column_sort="order", 
                                        column_id="device_id",
                                        max_timeshift=time,
                                        min_timeshift=time
                                        )

            #After rolling, I start Labelling each interval. 
            rolled_ids = df_rolled["id"].value_counts().index
            
            #Intialize y matrix
            y = pd.DataFrame(rolled_ids, columns = ["roll_id"])
            print("Finding the ID Labels:")

            #If an interval has a single reading below 0 then it would be considered freezing
            freezing_ids = df_rolled.groupby(['id']).min().temperature <= 0

            #If we have more than 10 readings between 0 and 2 in an interval, we consider that interval "cold"
            cold_indeces = df_rolled["temperature"].between(0,2)
            cold_df = df_rolled[cold_indeces].groupby(['id']).count() > 10
            cold_ids = cold_df[cold_df["temperature"] == True].index

            #This commented block can be uncommented if we want to change the conditions for cold and warm labels to full intervals rather than just 10 readings
            #cold_ids = df_rolled.groupby(['id']).max().temperature <= 2
            #warm_ids = df_rolled.groupby(['id']).min().temperature >= 8

            #If we have more than 10 readings between 8 and 25 in an interval, we consider that interval "warm"
            warm_indeces = df_rolled["temperature"].between(8,25)
            warm_df = df_rolled[warm_indeces].groupby(['id']).count() > 10
            warm_ids = warm_df[warm_df['temperature'] == True].index

            #If an interval has a single reading above 25 then it would be considered hot
            hot_ids = df_rolled.groupby(['id']).max().temperature > 25

            #This finally puts all the above together
            print("Building the y matrix:")
            y['seq_id'] = y['roll_id'].astype(str).str.split(r'\D',expand = True)[3].astype('int')
            y = y.sort_values("seq_id",ascending = True)
            y = y.set_index('roll_id')
            y["label"] = "ideal"
            y.loc[warm_ids,"label"] = "warm"
            y.loc[hot_ids,"label"] = "hot"
            y.loc[cold_ids,"label"] = "cold"
            y.loc[freezing_ids,"label"] = "freezing"

           # y_5hr = pd.read_hdf(y_5hr_dir,key='y')
           # y_5hr = y_5hr.drop("label_shifted",axis='columns')
            #Shift the labels to see if we can forecast the next interval's label using data from the current interval
            y["label_shifted"] = y["label"].shift(-(time+1))
            y["seq_shifted"] = y["seq_id"].shift(-(time+1))
           # y_5hr["label_shifted"] = y.loc[y_5hr.index,["label_shifted"]]

            print("Saving")
            df_rolled.to_hdf(df_directory,key='df')
            y.to_hdf(y_var_dir, key='y')