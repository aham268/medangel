import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.feature_extraction import extract_features
import tsfresh

if __name__ == '__main__':

    #Loading in and Cleaning data
    df = preprocessing.load_relevant_data()
    df['diff'] = df.groupby("device_id")['temperature'].diff().fillna(0)
    df = df.drop(columns=["doy_id"])
    print(df.head(10))
    #Preparing Loop to go through each device
    devices = df["device_id"].value_counts()
    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()
    features_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\features_with_diff.h5'
    run = 0
    #Loop through each device in the data
    for device in devices.index:

        #Intialize save directories for rolled dataframe and labels
        print("Working on device: " + str(device))
        
        #temperory df of only values corresponding to the correct device
        df_temp = df[df["device_id"] == device]

        #Sorting values based on datetime, this is to add an "Order" column because tsfresh seems to benefit from this rather than just having a sorted column
        df_temp = df_temp.sort_values("datetime")
        df_temp["order"] = range(df_temp.shape[0])
        print("Rolling the data:")

        #Roll the time series using tsfresh functionality. I use a max_timeshift and min_timeshift of 99 because this makes the timeseries roll only 100 values (index begins at 0)
        df_rolled = roll_time_series(
                                    df_temp,
                                    column_sort="order", 
                                    column_id="device_id",
                                    max_timeshift=99,
                                    min_timeshift=99
                                    )
        temp_features = extract_features(df_rolled, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value="diff")
        
        if run == 0:
            features = temp_features
            run = 1
        else:
            features = features.append(temp_features)

        
    print("Saving")
    features.to_hdf(features_directory, key='df')