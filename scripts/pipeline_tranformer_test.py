from medangel.models import pipeline_builder
import tsfresh
import pandas as pd
import numpy as np

if __name__ == "__main__":
    df_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_52040_20.h5'
    df = pd.read_hdf(df_directory,key='df')
    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()
    features = tsfresh.feature_extraction.extract_features(df, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value = "temperature")
    transformer = pipeline_builder.CollinearityTransformer()
    features_transformed = transformer.transform(features)
    print(features_transformed.head(10))
