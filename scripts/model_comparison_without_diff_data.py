import pandas as pd
import numpy as np

from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation

from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import LeaveOneGroupOut
from sklearn.metrics import matthews_corrcoef

from imblearn.pipeline import Pipeline
from imblearn.under_sampling import RandomUnderSampler

from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.transformers import FeatureSelector

from lightgbm import LGBMClassifier

import re

import time

if __name__ == '__main__':

    devices = preprocessing.load_device_ids()
    features,y = preprocessing.load_feature_data()
    features = features.drop([49103,47683],level = 0)
    y = y.loc[features.index]
    # features_diff = pd.read_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\features_with_diff.h5',key='df')
    # features_diff = features_diff.dropna(axis=1)
    # features_diff = features_diff.loc[features.index]
    # features_with_diff = pd.concat([features, features_diff], axis=1, join="inner")

    

    x_array = np.array(features.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))

    # x_array_diff =  np.array(features_with_diff.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x)))
    y_array = np.array(y["label_shifted"])

    fit_times = []
    pred_times = []
    test_group = []
    scores = []
    title = []

    groups = np.array([value[0] for value in features.index])
    
    logo = LeaveOneGroupOut()

    #no_diff_param = {'classifier__num_leaves': 60,
    #'classifier__n_estimators': 250,
    #'classifier__max_depth': 6,
    #'classifier__boosting_type': 'goss',
    #'augmenter__fdr_level': 0.03}
    classifier__num_leaves = 80
    classifier__n_estimators = 250
    classifier__max_depth = 7
    classifier__boosting_type = 'goss'
    augmenter__fdr_level = 0.03

    # diff_param = 'blank'

    lgbm_clf_no_diff = Pipeline([
        ('sampler',RandomUnderSampler(random_state=42)),
        ('augmenter',FeatureSelector(fdr_level=augmenter__fdr_level,n_jobs=4)),
        ('classifier',LGBMClassifier(random_state=42,n_jobs=4,device_type = 'gpu',max_bin = 64,num_leaves=classifier__num_leaves,
        n_estimators=classifier__n_estimators,max_depth=classifier__max_depth,boosting_type=classifier__boosting_type)),
    ])

    for i in range(25):
        for train_index,test_index in logo.split(features,y,groups):
            start = time.time()

            x_train,x_test = x_array[train_index],x_array[test_index]
            y_train,y_test = y_array[train_index],y_array[test_index]

            test_device = list(set(groups[test_index]))

            lgbm_clf_no_diff.fit(x_train,y_train)
            fit_times.append(time.time() - start)

            start = time.time()

            pred_no_diff = lgbm_clf_no_diff.predict(x_test)
            pred_times.append(time.time() - start)

            scores.append(matthews_corrcoef(y_test,pred_no_diff))

            title.append('no_diff')
            test_group.append(test_device[0])




    df_dict = {'title':title,
            'test_device':test_group,
            'scores':scores,
            'fit_times':fit_times,
            'pred_times':pred_times}
    df_scores = pd.DataFrame(df_dict)
    print(df_scores.head(10))
    df_scores.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\diff_comparison_data_v2.h5',key="df")





        

        