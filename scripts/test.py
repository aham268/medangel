import pandas as pd
from medangel.data import preprocessing
from medangel.features import labelling
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series

if __name__ == '__main__':
    df = preprocessing.load_relevant_data()
    df_temp = df[df["device_id"] == 59497]
    df_temp = df_temp.sort_values("datetime")
    df_temp["order"] = range(df_temp.shape[0])
    df_rolled = roll_time_series(df_temp,column_sort="order", column_id="device_id",
                                max_timeshift=99,
                                min_timeshift=99)
    rolled_ids = df_rolled["id"].value_counts().index
    y = pd.DataFrame(rolled_ids, columns = ["roll_id"])
    y["label"] = labelling.rolling_labels_speed(y["roll_id"], y,  df_rolled)