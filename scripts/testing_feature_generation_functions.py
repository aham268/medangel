import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series

if __name__ == '__main__':
    df = preprocessing.load_relevant_data()

    devices = preprocessing.load_device_ids()


    for device in devices:
        df_temp = df[df.device_id == device]

        #features = feature_creation.rolled_feature_creation(df_temp)
        y = feature_creation.label_creation(df_temp)

        #features.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolled_data__' + str(device) + '.h5',key='df')
        y.to_hdf(r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\y_data_30_min_interval_' + str(device) + '.h5',key='df')