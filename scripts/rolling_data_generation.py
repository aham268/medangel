import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series

done = [48790,49444,49486,59497]
if __name__ == '__main__':
    df = preprocessing.load_relevant_data()

    devices = df["device_id"].value_counts()

    for device in devices.index:
        if device in done:
            pass  
        df_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_data_device_' + str(device) + '.h5'
        y_directory = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\rolling_labels_shifted_device_' + str(device) + '.h5'
        print("Working on device: " + str(device))
        df_temp = df[df["device_id"] == device]
        df_temp = df_temp.sort_values("datetime")
        df_temp["order"] = range(df_temp.shape[0])
        df_rolled = roll_time_series(df_temp,column_sort="order", column_id="device_id",
                                max_timeshift=99,
                                min_timeshift=99)
        rolled_ids = df_rolled["id"].value_counts().index
        y = pd.DataFrame(rolled_ids, columns = ["roll_id"])
        y["label"] = y.apply(lambda x: preprocessing.rolling_labels(x["roll_id"],df_rolled),axis=1)
        y['seq_id'] = y['roll_id'].astype(str).str.split(r'\D',expand = True)[3].astype('int')
        y = y.sort_values("seq_id",ascending = True)
        y = y.reset_index(drop=True)
        y["label_shifted"] = y["label"].shift(-100)
        y = y.dropna()

        
        df_rolled.to_hdf(df_directory, key='df')
        y.to_hdf(y_directory, key='y')