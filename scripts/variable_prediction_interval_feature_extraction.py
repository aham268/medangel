import pandas as pd
from medangel.data import preprocessing
from medangel.models import model_builder
from medangel.features import feature_creation
import numpy as np
from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.feature_extraction import extract_features
import tsfresh

if __name__ == '__main__':

    ### roll data -> extract features -> save -> repeat for each time frame
    
    #Loading in and Cleaning data
    df = preprocessing.load_relevant_data()
    settings =  tsfresh.feature_extraction.ComprehensiveFCParameters()

    devices = preprocessing.load_device_ids()

    timescale = [159,179]

    for time in timescale:
    #Loop through each device in the data
        features = pd.DataFrame()
        features_dir = r'C:\Users\AliHa\Desktop\Masters Project\medangel\data\preprocessed\features_prediction_interval' + str(time) + '.h5'
        for device in devices:
           
            print("Working on device: " + str(device))
            
            #temperory df of only values corresponding to the correct device
            df_temp = df[df["device_id"] == device]

            #Sorting values based on datetime, this is to add an "Order" column because tsfresh seems to benefit from this rather than just having a sorted column
            df_temp = df_temp.sort_values("datetime")
            df_temp["order"] = range(df_temp.shape[0])
            print("Rolling the data:")


            df_rolled = roll_time_series(
                                        df_temp,
                                        column_sort="order", 
                                        column_id="device_id",
                                        max_timeshift=time,
                                        min_timeshift=time,
                                        n_jobs=5
                                        )

            df_temp = pd.DataFrame()
            temp_features = extract_features(df_rolled, 
                                                column_id = "id",
                                                column_sort = "datetime",
                                                default_fc_parameters=settings,
                                                column_value="temperature",
                                                n_jobs=5)
            df_rolled = pd.DataFrame()
            
            features = features.append(temp_features)
            temp_features = pd.DataFrame()

        features.to_hdf(features_dir,key="df")


